import instance from "../utils/Service";
const Api = {
  // SubDistribution Location
  fetchsubdistributionlocation: () => {
    const url = "/subdistributionLocations";
    return instance.get(url);
  },
  createsubdistributionlocation: value => {
    const url = "/SubdistributionLocations";
    return instance.post(url, value.data);
  },
  updatesubdistributionlocation: value => {
    const url = "/SubdistributionLocations";
    return instance.patch(url, value.data);
  },
  deletesubdistributionlocation: value => {
    console.log(
      "API => deletedsubdistributionlocation()  recieved value is ",
      value
    );
    const url = `/SubdistributionLocations/${value.data.id}`;
    return instance.delete(url);
  }
};

export default Api;
