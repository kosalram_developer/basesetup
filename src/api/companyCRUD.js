import instance from "../utils/Service";

// Alter defaults after instance has been created
// instance.defaults.headers.common['Authorization'] = AUTH_TOKEN;

const Api = {
  //company
  fetchformcompany: () => {
    const url = "/Companies";
    return instance.get(url);
  },
  fetchcompany: () => {
    const url = "/Companies/fetch";
    return instance.get(url);
  },
  createcompany: value => {
    const url = "/Companies/create";
    return instance.post(url, value.data);
  },
  updatecompany: value => {
    const url = "/Companies/update";
    return instance.put(url, value.data); //post for mokeable purpose change to patch
  },
  deletecompany: value => {
    console.log(
      "API => deletecompany()  recieved value is ",
      value.data.company.id
    );
    const url = `/Companies/${value.data.company.id}`;
    // const url = `/Companies`;
    return instance.delete(url);
  }
};

export default Api;
