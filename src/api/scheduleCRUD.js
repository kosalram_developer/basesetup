import instance from "../utils/Service";
const Api = {
  //schedule
  fetchschedule: () => {
    const url = "/Schedules";
    return instance.get(url);
  },
  createschedule: value => {
    const url = "/Schedules";
    return instance.post(url, value.data);
  },
  updateschedule: value => {
    const url = "/Schedules";
    return instance.patch(url, value.data);
  },
  deleteschedule: value => {
    console.log("API => deleteschedule()  recieved value is ", value);
    const url = `/Schedules/${value.data.id}`;
    return instance.delete(url);
  }
};

export default Api;
