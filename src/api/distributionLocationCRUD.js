import instance from "../utils/Service";
// Distribution Location
const Api = {
  fetchdistributionlocation: () => {
    const url = "/DistributionLocations";
    return instance.get(url);
  },
  createdistributionlocation: value => {
    const url = "/DistributionLocations";
    return instance.post(url, value.data);
  },
  updatedistributionlocation: value => {
    console.log(
      "API => updatedistributionlocation()  recieved value is ",
      value
    );
    const url = "/DistributionLocations";
    return instance.patch(url, value.data);
  },
  deletedistributionlocation: value => {
    console.log(
      "API => deletedistributionlocation()  recieved value is ",
      value
    );
    const url = `/DistributionLocations/${value.data.id}`;
    return instance.delete(url);
  }
};
export default Api;
