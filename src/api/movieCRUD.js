import instance from "../utils/Service";

const Api = {
  fetchMovie: () => {
    const url = "/Movies";
    return instance.get(url);
  },
  createMovie: value => {
    const url = "/Movies";
    return instance.post(url, value.data);
  },
  updateMovie: value => {
    const url = "/Movies";
    return instance.put(url, value.data); //post for mokeable purpose change to patch
  },
  deleteMovie: value => {
    const url = `/Movies`;
    return instance.delete(url);
  }
};

export default Api;
