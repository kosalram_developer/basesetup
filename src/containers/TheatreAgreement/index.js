import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
// import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateTheatreAgreement from "./TheatreAgreementCreate";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  ADD_NEW_AGREEMENT,
  UPDATE_AGREEMENT,
  DELETE_AGREEMENT,
  FETCH_AGREEMENT,
  FETCH_FORM_COMPANY,
  FETCH_MOVIE,
  FETCH_THEATRE,
  FETCH_SUBDISTRIBUTIONLOCATION
} from "../../actions/actionConstants";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { editAgreement, clearAgreement } from "../../actions/admin";

class TheatreAgreement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const {
      fetchTheatreAgreement,
      fetchSubDistributionLocation,
      fetchMovie,
      fetchCompany,
      fetchTheatre
    } = this.props;
    fetchTheatreAgreement();
    fetchSubDistributionLocation();
    fetchMovie();
    fetchCompany();
    fetchTheatre();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    const { createTheatreAgreement, updateTheatreAgreement } = this.props;
    console.log(
      "StationLocaion container => before value.id submitAction",
      value
    );
    let shares = [];

    value.share.map(val => {
      shares.push(val.value);
    });
    var createdDate = new Date(value.created_date).toDateString();
    const updatedValue = {
      share: shares,
      tax: value.tax,
      is_air_conditioned: value.ac,
      type: value.type.value,
      created_date: createdDate,
      id: value.id,
      movie_id: value.movie.id,
      theater_id: value.theater.id,
      company_id: value.company.id
    };
    console.log(
      "StationLocaion container => updateTheatreAgreement()",
      updatedValue
    );
    if (value.id) {

      updateTheatreAgreement(updatedValue);
    } else {
      createTheatreAgreement(updatedValue);
    }

    this.setState({ openForm: false });
  };
  handleEdit = rowData => {
    console.log(
      "Station Location Container => index => handleEdit()",
      rowData.original
    );
    const { editTheatreAgreementDetails } = this.props;
    editTheatreAgreementDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    console.log("Station Location Container => index => handleDete()", rowData);
    const { deleteTheatreAgreementDetails } = this.props;
    deleteTheatreAgreementDetails(rowData.original);
  };
  render() {
    const {
      theatreAgreementList,
      movieList,
      companyList,
      theatreList,
      classes
    } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = theatreAgreementList.length
      ? theatreAgreementList.length
      : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createTheatreAgreement}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.theatreAgreementContainor}>
          <ReactTable
            data={theatreAgreementList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 20
              },
              {
                Header: "Theatre ",
                className: classes.eachRow,
                accessor: "theater.name"
              },
              {
                Header: "Tax ",
                className: classes.eachRow,
                accessor: "tax"
              },
              {
                Header: "Share",
                className: classes.eachRow,
                accessor: "share"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.theatreAgreementTable}`}
          />
        </div>

        <CreateTheatreAgreement
          openForm={openForm}
          movieList={movieList}
          companyList={companyList}
          theatreList={theatreList}
          closeForm={this.closeForm}
          initialValue={initialValue}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

TheatreAgreement.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  theatreAgreementList: PropTypes.array.isRequired,
  theatreList: PropTypes.array.isRequired,
  movieList: PropTypes.array.isRequired,
  companyList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  theatreAgreementList: state.theatreAgreementCRUD.agreementList,
  theatreList: state.theatreCRUD.theatreList,
  movieList: state.movieCRUD.movieList,
  companyList: state.companyCRUD.companyFormList
});
const mapDispatchToProps = dispatch => ({
  fetchTheatreAgreement: () => {
    dispatch(action(FETCH_AGREEMENT));
  },
  fetchCompany: () => {
    dispatch(action(FETCH_FORM_COMPANY));
  },
  fetchMovie: () => {
    dispatch(action(FETCH_MOVIE));
  },
  fetchTheatre: () => {
    dispatch(action(FETCH_THEATRE));
  },
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  createTheatreAgreement: value => {
    dispatch(action(ADD_NEW_AGREEMENT, value));
  },
  updateTheatreAgreement: value => {
    dispatch(action(UPDATE_AGREEMENT, value));
  },
  editTheatreAgreementDetails: value => {
    dispatch(editAgreement(value));
  },
  clearTheatreAgreement: () => {
    dispatch(clearAgreement());
  },
  deleteTheatreAgreementDetails: value => {
    dispatch(action(DELETE_AGREEMENT, value));
  }
});
const TheatreAgreementCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(TheatreAgreement);

export default compose(withStyles(styles))(TheatreAgreementCmpt);
