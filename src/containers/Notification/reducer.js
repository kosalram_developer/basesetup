const ADD_NOTIFICATION = "ADD_NOTIFICATION";
const REMOVE_NOTIFICATION = "REMOVE_NOTIFICATION";
const REMOVE_ALL_NOTIFICATION = "REMOVE_ALL_NOTIFICATION";

const initialState = { notifications: [] };

let notificationKey = 1;

const notificaitons = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NOTIFICATION: {
      const { notifications } = state;
      return {
        ...state,
        notifications: [
          ...notifications,
          { ...action.payload, key: ++notificationKey },
        ],
      };
    }
    case REMOVE_NOTIFICATION: {
      const { notifications } = state;
      const { id } = action.payload;
      const index = notifications.findIndex(({ key }) => key === id);
      if (index === -1) {
        return state;
      }
      return {
        ...state,
        notifications: [
          ...notifications.slice(0, index),
          ...notifications.slice(index + 1),
        ],
      };
    }
    case REMOVE_ALL_NOTIFICATION: {
      return {
        ...initialState,
      };
    }
    default:
      return state;
  }
};

export const showNotification = (heading, content, umStyle, delayHide) => {
  return {
    type: ADD_NOTIFICATION,
    payload: { heading, content, umStyle, delayHide },
  };
};

export const showSuccess = (heading, content) => {
  return showNotification(heading, content, "success", 5000);
};

export const showError = (heading, content) => {
  return showNotification(heading, content, "error", 0);
};

export const hideNotification = id => {
  return { type: REMOVE_NOTIFICATION, payload: { id } };
};

export const hideAll = () => {
  return { type: REMOVE_ALL_NOTIFICATION };
};

export default notificaitons;
