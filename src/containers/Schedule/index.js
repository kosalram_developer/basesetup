import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
// import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ReactTable from "react-table";
import "react-table/react-table.css";
import moment from 'moment';

import CreateSchedule from "./ScheduleCreate";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  ADD_NEW_SCHEDULE,
  UPDATE_SCHEDULE,
  DELETE_SCHEDULE,
  FETCH_SCHEDULE,
  FETCH_MOVIE,
  FETCH_THEATRE,
  FETCH_FORM_COMPANY,
  FETCH_SUBDISTRIBUTIONLOCATION
} from "../../actions/actionConstants";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { editSchedule, clearSchedule } from "../../actions/admin";

class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const {
      fetchSchedule,
      fetchMovie,
      fetchCompany,
      fetchTheatre,
      fetchSubDistributionLocation
    } = this.props;
    fetchSchedule();
    fetchMovie();
    fetchCompany();
    fetchTheatre();
    fetchSubDistributionLocation();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    const { createSchedule, updateSchedule } = this.props;
    console.log(
      "StationLocaion container => before value.id submitAction",
      value
    );
    var fromDate = new Date(value.from_date).toDateString();
    var toDate = new Date(value.to_date).toDateString();
    const updatedValue = {
      theater_id: value.theater.id,
      movie_id: value.movie.id,
      producer_id: value.producer.id,
      distribution_id: value.distribution.id,
      subdistribution_id: value.subdistribution.id,
      subdistribution_location_id: value.subdistributionLocation.id,
      from_date: fromDate,
      to_date: toDate
    };
    if (value.id) {
      console.log("StationLocaion container => updateSchedule()", value);
      updateSchedule(updatedValue);
    } else {
      createSchedule(updatedValue);
    }

    this.setState({ openForm: false });
  };
  handleEdit = rowData => {
    console.log(
      "Station Location Container => index => handleEdit()",
      rowData.original
    );
    const { editScheduleDetails } = this.props;
    editScheduleDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    console.log("Station Location Container => index => handleDete()", rowData);
    const { deleteScheduleDetails } = this.props;
    deleteScheduleDetails(rowData.original);
  };
  render() {
    const {
      scheduleList,
      movieList,
      theatreList,
      companyList,
      subDistributionLocationList,
      classes
    } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = scheduleList.length ? scheduleList.length : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createSchedule}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.scheduleContainor}>
          <ReactTable
            data={scheduleList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    <DeleteIcon onClick={() => this.handleDelete(rowData)} />
                  </div>
                ),
                minWidth: 20
              },
              {
                Header: "Theatre",
                className: classes.eachRow,
                Cell: rowData => {
                  return (
                    <div>
                      {rowData.original.theater ? rowData.original.theater.name : ""}
                    </div>
                  );
                },
              },
              {
                Header: "Movie",
                className: classes.eachRow,
                Cell: rowData => {
                  return (
                    <div>
                      {rowData.original.movie ? rowData.original.movie.name : ""}
                    </div>
                  );
                },
              },
              {
                Header: "From Date",
                className: classes.eachRow,
                Cell: rowData => {
                  return (
                    <div>
                      {moment(rowData.original.from_date).format("DD-MM-YYYY")}
                    </div>
                  );
                },
              },
              {
                Header: "To Date",
                className: classes.eachRow,
                Cell: rowData => {
                  return (
                    <div>
                      {moment(rowData.original.to_date).format("DD-MM-YYYY")}
                    </div>
                  );
                },
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.scheduleTable}`}
          />
        </div>

        <CreateSchedule
          openForm={openForm}
          movieList={movieList}
          theatreList={theatreList}
          companyList={companyList}
          subDistributionLocationList={subDistributionLocationList}
          closeForm={this.closeForm}
          initialValue={initialValue}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

Schedule.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  scheduleList: PropTypes.array.isRequired,
  theatreList: PropTypes.array.isRequired,
  companyList: PropTypes.array.isRequired,
  movieList: PropTypes.array.isRequired,
  subDistributionLocationList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  scheduleList: state.scheduleCRUD.scheduleList,
  movieList: state.movieCRUD.movieList,
  companyList: state.companyCRUD.companyFormList,
  theatreList: state.theatreCRUD.theatreList,
  subDistributionLocationList:
    state.subDistributionLocationCRUD.subDistributionLocationList
});
const mapDispatchToProps = dispatch => ({
  fetchSchedule: () => {
    dispatch(action(FETCH_SCHEDULE));
  },
  fetchMovie: () => {
    dispatch(action(FETCH_MOVIE));
  },
  fetchCompany: () => {
    dispatch(action(FETCH_FORM_COMPANY));
  },
  fetchTheatre: () => {
    dispatch(action(FETCH_THEATRE));
  },
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  createSchedule: value => {
    dispatch(action(ADD_NEW_SCHEDULE, value));
  },
  updateSchedule: value => {
    dispatch(action(UPDATE_SCHEDULE, value));
  },
  editScheduleDetails: value => {
    dispatch(editSchedule(value));
  },
  clearSchedule: () => {
    dispatch(clearSchedule());
  },
  deleteScheduleDetails: value => {
    dispatch(action(DELETE_SCHEDULE, value));
  }
});
const ScheduleCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(Schedule);

export default compose(withStyles(styles))(ScheduleCmpt);
