const validate = values => {
  const errors = {};

  if (!values.name) {
    errors.name = "Required";
  }
  if (!values.mobile_number) {
    errors.mobile_number = "Required";
  }
  if (!values.phone_number) {
    errors.phone_number = "Required";
  }
  if (!values.manager_name) {
    errors.manager_name = "Required";
  }
  if (!values.door_number) {
    errors.door_number = "Required";
  }
  if (!values.street_name) {
    errors.street_name = "Required";
  }
  if (!values.area) {
    errors.area = "Required";
  }
  if (!values.city) {
    errors.city = "Required";
  }
  if (!values.pincode) {
    errors.pincode = "Required";
  }
  if (!values.station_name) {
    errors.station_name = "Required";
  }
  if (!values.type) {
    errors.type = "Required";
  }
  // if (!values.firstName) {
  //   errors.firstName = 'Required'
  // }
  // if (!values.lastName) {
  //   errors.lastName = 'Required'
  // }
  // if (!values.number) {
  //   errors.number = 'Required'
  // } else if(values.number.length < 10) {
  //   errors.number = 'Invalid'
  // }
  // if(!values.password) {
  //   errors.password = 'Required'
  // }
  if (!values.logo) {
    errors.logo = "Required";
  }
  // if (!values.email) {
  //   errors.email = "Required";
  // } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
  //   errors.email = "Invalid email address";
  // }

  return errors;
};

export default validate;
