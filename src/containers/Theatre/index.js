import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { reset } from "redux-form";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateTheatre from "./CreateTheatre";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  ADD_NEW_THEATRE,
  UPDATE_THEATRE,
  DELETE_THEATRE,
  FETCH_THEATRE,
  FETCH_DISTRIBUTIONLOCATION,
  FETCH_SUBDISTRIBUTIONLOCATION,
  FETCH_STATIONLOCATION
} from "../../actions/actionConstants";
import { editTheatre, clearTheatre } from "../../actions/admin";

class Theatre extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const {
      fetchTheatre,
      fetchSubDistributionLocation,
      fetchDistributionLocation,
      fetchStationLocation
    } = this.props;
    fetchTheatre();
    fetchSubDistributionLocation();
    fetchDistributionLocation();
    fetchStationLocation();
  }
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    console.log("submitAction", value);
    const updatedvalue = {
      name: value.name,
      mobile_number: value.mobile_number,
      phone_number: value.phone_number,
      manager_name: value.manager_name,
      station_name: value.station_name,
      door_number: value.door_number,
      street_name: value.street_name,
      area: value.area,
      city: value.city,
      pincode: value.pincode,
      is_air_conditioned: value.is_air_conditioned,
      type: value.type,
      lat: value.lat,
      long: value.long,
      distribution_location_id: value.distributionLocation.id,
      subdistribution_location_id: value.subdistributionLocation.id,
      station_id: value.station.id
    };
    const { createTheatre, updateTheatre } = this.props;
    if (value.id) {
      updateTheatre(updatedvalue);
    } else {
      console.log("value from Theatre component", updatedvalue);
      createTheatre(updatedvalue);
    }
    this.props.clearTheatre();
    this.setState({ openForm: false });
  };
  handleEdit = rowData => {
    console.log("Theatre->index.js=>handleEdit(", rowData.original);
    const { editTheatreDetails } = this.props;
    editTheatreDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    console.log("Login Container => index => handleDete()", rowData);
    const { deleteTheatreDetails } = this.props;
    deleteTheatreDetails(rowData.original);
  };
  render() {
    const {
      theatreList,
      subDistributionLocationList,
      distributionLocationList,
      stationLocationList,
      classes
    } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = theatreList.length ? theatreList.length : 3;
    console.log("theatreList.length");
    console.log(theatreList);

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createTheatre}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.theatreContainor}>
          <ReactTable
            data={theatreList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    <DeleteIcon onClick={() => this.handleDelete(rowData)} />
                  </div>
                ),
                minWidth: 20
              },
              {
                Header: "Name",
                className: classes.eachRow,
                accessor: "name"
              },
              {
                Header: "Manager Name",
                accessor: "manager_name"
              },
              {
                Header: "Number",
                accessor: "phone_number"
              },
              {
                Header: "Area",
                accessor: "area"
              },
              {
                Header: "City",
                accessor: "city"
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.theatreTable}`}
          />
        </div>

        <CreateTheatre
          distributionLocationList={distributionLocationList}
          subDistributionLocationList={subDistributionLocationList}
          stationLocationList={stationLocationList}
          openForm={openForm}
          initialValue={initialValue}
          closeForm={this.closeForm}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

Theatre.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  theatreList: PropTypes.array.isRequired,
  distributionLocationList: PropTypes.array.isRequired,
  subDistributionLocationList: PropTypes.array.isRequired,
  stationLocationList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  subDistributionLocationList:
    state.subDistributionLocationCRUD.subDistributionLocationList,
  theatreList: state.theatreCRUD.theatreList,
  stationLocationList: state.stationLocationCRUD.stationLocationList,

  distributionLocationList:
    state.distributionLocationCRUD.distributionLocationList
});
const mapDispatchToProps = dispatch => ({
  fetchTheatre: () => {
    dispatch(action(FETCH_THEATRE));
  },
  fetchDistributionLocation: () => {
    dispatch(action(FETCH_DISTRIBUTIONLOCATION));
  },
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  fetchStationLocation: () => {
    dispatch(action(FETCH_STATIONLOCATION));
  },
  createTheatre: value => {
    dispatch(action(ADD_NEW_THEATRE, value));
  },
  updateTheatre: value => {
    dispatch(action(UPDATE_THEATRE, value));
  },
  editTheatreDetails: value => {
    dispatch(editTheatre(value));
  },
  clearTheatre: () => {
    dispatch(clearTheatre());
  },
  deleteTheatreDetails: value => {
    dispatch(action(DELETE_THEATRE, value));
  }
});
const TheatreCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(Theatre);

export default compose(withStyles(styles))(TheatreCmpt);
