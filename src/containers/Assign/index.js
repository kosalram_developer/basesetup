import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ReactTable from "react-table";
import "react-table/react-table.css";

import AssignMovie from "./AssignMovie";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  FETCH_FORM_COMPANY,
  ADD_NEW_ASSIGN,
  UPDATE_COMPANY,
  DELETE_COMPANY,
  FETCH_MOVIE,
  FETCH_DISTRIBUTIONLOCATION,
  FETCH_SUBDISTRIBUTIONLOCATION,
  FETCH_ASSIGN
} from "../../actions/actionConstants";
import { editAssign, clearAssign } from "../../actions/admin";

class Assign extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const {
      fetchAssign,
      fetchCompany,
      fetchMovie,
      fetchDistributionLocation,
      fetchSubDistributionLocation
    } = this.props;
    fetchAssign();
    fetchCompany();
    fetchMovie();
    fetchDistributionLocation();
    fetchSubDistributionLocation();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    const { clearAssign } = this.props;
    this.setState({ openForm: true });
    clearAssign();
  };
  submitAction = value => {
    console.log("Movie ASSIGN Submit value is", value);
    const { createAssign, updateAssign } = this.props;
    // class Producer {
    //   let company_id;
    //   let movie_id;
    // };
    const movieId = value.movie.id;
    let submitValue = {};
    submitValue.movie_id = movieId;
    submitValue.producer = {};
    submitValue.distribution = [];
    submitValue.subDistribution = [];
    submitValue.producer.company_id = value.producer.id;
    submitValue.producer.movie_id = movieId;
    value.distribution.map(dist => {
      submitValue.distribution.push({ distribution_location_id: dist.areaId.id, company_id: dist.companyId.id })
    })
    value.subDistribution.map(subdist => {
      submitValue.subDistribution.push({ Subdistribution_location_id: subdist.areaId.id, company_id: subdist.companyId.id })
    });
    console.log('submitValue============================');
    console.log(submitValue);

    if (value.id) {
      updateAssign(submitValue);
    } else {
      createAssign(submitValue);
    }
    this.closeForm();
  };
  handleEdit = rowData => {
    const { editAssignDetails } = this.props;
    editAssignDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    const { deleteAssignDetails } = this.props;
    deleteAssignDetails(rowData.original);
  };
  render() {
    const {
      assignList,
      companyList,
      movieList,
      subDistributionLocationList,
      distributionLocationList,
      classes
    } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = assignList.length ? assignList.length : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createAssign}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.assignContainor}>
          <ReactTable
            data={assignList.producer}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 40
              },
              {
                Header: "Producer",
                className: classes.eachRow,
                accessor: "company.name"
              },
              {
                Header: "Movie",
                className: classes.eachRow,
                accessor: "movie.name"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.companyTable}`}
          />
        </div>
        <div className={classes.assignContainor}>
          <ReactTable
            data={assignList.distribution}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 40
              },
              {
                Header: "Distribution Area",
                className: classes.eachRow,
                accessor: "distributionLocation.name"
              },
              {
                Header: "Company",
                className: classes.eachRow,
                accessor: "company.name"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.companyTable}`}
          />
        </div>

        <div className={classes.assignContainor}>
          <ReactTable
            data={assignList.subDistribution}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 40
              },
              {
                Header: "SubDistribution Area",
                className: classes.eachRow,
                accessor: "subdistributionLocation.name"
              },
              {
                Header: "Company",
                className: classes.eachRow,
                accessor: "company.name"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.companyTable}`}
          />
        </div>

        <AssignMovie
          companyList={companyList}
          movieList={movieList}
          distributonAreaList={distributionLocationList}
          subdistributonAreaList={subDistributionLocationList}
          openForm={openForm}
          initialValue={initialValue}
          closeForm={this.closeForm}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

Assign.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  companyList: PropTypes.array.isRequired,
  movieList: PropTypes.array.isRequired,
  distributionLocationList: PropTypes.array.isRequired,
  subDistributionLocationList: PropTypes.array.isRequired,
  assignList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  companyList: state.companyCRUD.companyFormList,
  movieList: state.movieCRUD.movieList,
  distributionLocationList:
    state.distributionLocationCRUD.distributionLocationList,
  subDistributionLocationList:
    state.subDistributionLocationCRUD.subDistributionLocationList,
  assignList: state.assignCRUD.assignList
});
const mapDispatchToProps = dispatch => ({
  fetchAssign: () => {
    dispatch(action(FETCH_ASSIGN));
  },
  fetchCompany: () => {
    dispatch(action(FETCH_FORM_COMPANY));
  },
  fetchMovie: () => {
    dispatch(action(FETCH_MOVIE));
  },
  fetchDistributionLocation: () => {
    dispatch(action(FETCH_DISTRIBUTIONLOCATION));
  },
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  createAssign: value => {
    dispatch(action(ADD_NEW_ASSIGN, value));
  },
  updateAssign: value => {
    dispatch(action(UPDATE_COMPANY, value));
  },
  editAssignDetails: value => {
    dispatch(editAssign(value));
  },
  clearAssign: () => {
    dispatch(clearAssign());
  },
  deleteAssignDetails: value => {
    dispatch(action(DELETE_COMPANY, value));
  }
});
const AssignCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(Assign);

export default compose(withStyles(styles))(AssignCmpt);
