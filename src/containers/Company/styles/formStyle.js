const formStyle = theme => ({
    companyForm:{
        padding:'20px 50px',
        overflow: 'auto',
        height: '490px',
    },
    fields:{
        padding:'10px 0',
        margin:'auto'
    },
    field:{
        width:'100%'
    },
    submitButton:{
        backgroundColor:'#2196f3',
        color:'white',
        margin:'0 20px',
        '&:hover': {
            backgroundColor:'#57adf2',
          }
    },
    cleatButton:{
        backgroundColor:'rgb(157, 0, 56)',
        color:'white',
        '&:hover': {
            backgroundColor:'#d33970',
          }
    }
});

export default formStyle;
