import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateCompany from "./CreateCompany";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  FETCH_COMPANY,
  ADD_NEW_COMPANY,
  UPDATE_COMPANY,
  DELETE_COMPANY
} from "../../actions/actionConstants";
import { editCompany, clearCompany } from "../../actions/admin";

class Company extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const { fetchCompany } = this.props;
    fetchCompany();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    const { clearCompany } = this.props;
    this.setState({ openForm: true });
    clearCompany();
  };
  submitAction = value => {
    const { createCompany, updateCompany } = this.props;
    if (value.company.id) {
      updateCompany(value);
    } else {
      createCompany(value);
    }
    this.closeForm();
  };
  handleEdit = rowData => {
    const { editCompanyDetails } = this.props;
    editCompanyDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    const { deleteCompanyDetails } = this.props;
    deleteCompanyDetails(rowData.original);
  };
  render() {
    const { companyList, classes } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = companyList.length ? companyList.length : 3;

    return (
      <div className={classes.company_container}>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createCompany}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.companyContainor}>
          <ReactTable
            data={companyList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 40
              },
              {
                Header: "Company",
                className: classes.eachRow,
                accessor: "company.name"
              },
              {
                Header: "Number",
                className: classes.eachRow,
                accessor: "company.phone_number"
              },
              {
                Header: "Email",
                className: classes.eachRow,
                accessor: "company.email"
              },
              {
                Header: "Website",
                className: classes.eachRow,
                accessor: "company.website"
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.companyTable}`}
          />
        </div>

        <CreateCompany
          openForm={openForm}
          initialValue={initialValue}
          closeForm={this.closeForm}
          onSubmit={this.submitAction}
        />
      </div>
    );
  }
}

Company.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  companyList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  companyList: state.companyCRUD.companyList
});
const mapDispatchToProps = dispatch => ({
  fetchCompany: () => {
    dispatch(action(FETCH_COMPANY));
  },
  createCompany: value => {
    dispatch(action(ADD_NEW_COMPANY, value));
  },
  updateCompany: value => {
    dispatch(action(UPDATE_COMPANY, value));
  },
  editCompanyDetails: value => {
    dispatch(editCompany(value));
  },
  clearCompany: () => {
    dispatch(clearCompany());
  },
  deleteCompanyDetails: value => {
    dispatch(action(DELETE_COMPANY, value));
  }
});
const CompanyCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(Company);

export default compose(withStyles(styles))(CompanyCmpt);
