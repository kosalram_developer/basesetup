import React from "react";
import { Field, reduxForm, FormSection, reset } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";

import { renderTextField } from "../../components/FormFields";

const CreateCompanyForm = props => {
  const {
    initialValues,
    handleSubmit,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;
  let isEdit = false;
  
  if(initialValues.company){
    if(initialValues.company.id) {
      isEdit = true;
    }
  }
  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      
      <form className={classes.companyForm} onSubmit={handleSubmit(onSubmit)}>
        <FormSection name="company">
          <div className={classes.fields}>
            <Field className={classes.field} name="name" component={renderTextField} label="Company Name" />
          </div>
          <div className={classes.fields}>
            <Field className={classes.field} name="phone_number" component={renderTextField} type='number' label="Phone Number" />
          </div>
          <div className={classes.fields}>
            <Field className={classes.field} name="email" component={renderTextField} type='email' label="Company Email" />
          </div>
          <div className={classes.fields}>
            <Field className={classes.field}
              name="address"
              multiline
              rowsMax="4"
              component={renderTextField}
              label="Address"
            />
          </div>
          <div className={classes.fields}>
            <Field className={classes.field} name="website" component={renderTextField} label="Website" />
          </div>
        </FormSection>
        
        <FormSection name="user">
          <div className={classes.fields}>
              <Field className={classes.field} name="name" component={renderTextField} label="Name"/>
          </div>
          <div className={classes.fields}>
              <Field className={classes.field} name="username" type="number" component={renderTextField} label="Mobile Number"/>
          </div>
          <div className={classes.fields}>
              <Field className={classes.field} name="email" type="email" component={renderTextField} label="Email"/>
          </div>
          {
            !isEdit &&
            <>
            <div className={classes.fields}>
              <Field className={classes.field} name="password" type="password" component={renderTextField} label="Password"/>
            </div>
            <div className={classes.fields}>
              <Field className={classes.field} name="confirmPassword" type="password" component={renderTextField} label="Confirm Password"/>
            </div>
            </>
          }
        </FormSection>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

const afterSubmit = (result, dispatch) =>
  dispatch(reset('CreateCompany'));

let CompanyForm = reduxForm({
  form: "CreateCompany", // a unique identifier for this form
  onSubmitSuccess: afterSubmit,
  validate,
  enableReinitialize: true
})(CreateCompanyForm);

CompanyForm = connect(state => ({
  initialValues: state.companyCRUD.companyEdit || {}
}))(CompanyForm);
export default compose(withStyles(formStyle))(CompanyForm);
