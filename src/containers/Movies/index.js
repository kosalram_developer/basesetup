import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateMovie from "./CreateMovie";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  FETCH_MOVIE,
  ADD_NEW_MOVIE,
  UPDATE_MOVIE,
  DELETE_MOVIE
} from "../../actions/actionConstants";
import { editMovie, clearMovie } from "../../actions/admin";

class Movie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false
    };
  }
  componentDidMount() {
    const { fetchMovie } = this.props;
    fetchMovie();
  }

  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    const { clearMovie } = this.props;
    this.setState({ openForm: true });
    clearMovie();
  };
  submitAction = value => {
    const { createMovie, updateMovie } = this.props;
    let technicians = [];
    let actors = [];
    value.technicians.map(val => {
      technicians.push(val.value);
    });
    value.actors.map(val => {
      actors.push(val.value);
    });
    var date = new Date(value.release_date).toDateString();
    const submitData = { ...value, technicians, actors, release_date: date };

    if (value.id) {
      updateMovie(submitData);
    } else {
      createMovie(submitData);
    }
    this.closeForm();
  };
  handleEdit = rowData => {
    const { editMovieDetails } = this.props;
    editMovieDetails(rowData.original);
    this.setState({ openForm: true });
  };
  handleDelete = rowData => {
    const { deleteMovieDetails } = this.props;
    deleteMovieDetails(rowData.original);
  };
  render() {
    const { movieList, classes } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = movieList.length ? movieList.length : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createCompany}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.companyContainor}>
          <ReactTable
            data={movieList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 40
              },
              {
                Header: "Movie",
                className: classes.eachRow,
                accessor: "name"
              },
              {
                Header: "Release Date",
                className: classes.eachRow,
                accessor: "release_date"
              },
              {
                Header: "Producer",
                className: classes.eachRow,
                accessor: "producer_name"
              },
              {
                Header: "Trailer",
                className: classes.eachRow,
                accessor: "trailer_link"
              },

              {
                Header: "Actors",
                className: classes.eachRow,
                accessor: "actors"
              },
              {
                Header: "Technicians",
                className: classes.eachRow,
                accessor: "technicians"
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.companyTable}`}
          />
        </div>

        <CreateMovie
          openForm={openForm}
          initialValue={initialValue}
          closeForm={this.closeForm}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

Movie.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  movieList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  movieList: state.movieCRUD.movieList
});
const mapDispatchToProps = dispatch => ({
  fetchMovie: () => {
    dispatch(action(FETCH_MOVIE));
  },
  createMovie: value => {
    dispatch(action(ADD_NEW_MOVIE, value));
  },
  updateMovie: value => {
    dispatch(action(UPDATE_MOVIE, value));
  },
  editMovieDetails: value => {
    dispatch(editMovie(value));
  },
  clearMovie: () => {
    dispatch(clearMovie());
  },
  deleteMovieDetails: value => {
    dispatch(action(DELETE_MOVIE, value));
  }
});
const MovieCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(Movie);

export default compose(withStyles(styles))(MovieCmpt);
