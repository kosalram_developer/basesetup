import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";

import {
  renderTextField,
  DateTimePickerRow,
  renderCreatableSelect
} from "../../components/FormFields";

const CreateMovieForm = props => {
  const {
    handleSubmit,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;

  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      <form className={classes.companyForm} onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="name"
            component={renderTextField}
            label="Movie Name"
          />
        </div>
        <div className={classes.fields}>
          <Field
            name="release_date"
            component={DateTimePickerRow}
            label="Release date"
            dateFormat="dd/MM/yyyy"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="producer_name"
            component={renderTextField}
            label="Producer Name"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="trailer_link"
            component={renderTextField}
            label="Trailer Link"
          />
        </div>
        <div className={classes.fields}>
          <Field
            name="actors"
            component={renderCreatableSelect}
            label="Actors"
          />
        </div>
        <div className={classes.fields}>
          <Field
            name="technicians"
            component={renderCreatableSelect}
            label="Technicians"
          />
        </div>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let MovieForm = reduxForm({
  form: "CreateMovie", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(CreateMovieForm);

MovieForm = connect(state => ({
  initialValues: state.movieCRUD.movieEdit || {}
}))(MovieForm);
export default compose(withStyles(formStyle))(MovieForm);
