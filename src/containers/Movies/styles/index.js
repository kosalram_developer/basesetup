const styles = theme => ({
  companyContainor: {
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  companyTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createCompany: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
