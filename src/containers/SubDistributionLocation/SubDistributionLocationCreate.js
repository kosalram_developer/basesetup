import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";
import {
  renderTextField,
  renderSelectField
} from "../../components/FormFields";

const MaterialUiForm = props => {
  const {
    handleSubmit,
    distributionLocationList,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;

  let distributionLocations = [];
  console.log("DistributionLocation List", distributionLocationList);
  distributionLocationList.map(value => {
    distributionLocations.push({
      label: value.name,
      value: value.id
    });
  });

  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      <form
        className={classes.subDistributionLocationForm}
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className={classes.fields}>
          <Field
            name="name"
            component={renderTextField}
            label="SubDistributionLocation Name"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="distributionLocation"
            component={renderSelectField}
            options={distributionLocationList}
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
            label="Select"
          />
        </div>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid || pristine || submitting}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let SubDistributionLocationForm = reduxForm({
  form: "CreateSubDistributionLocation", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(MaterialUiForm);

SubDistributionLocationForm = connect(state => ({
  initialValues: state.subDistributionLocationCRUD.subDistributionLocationEdit
}))(SubDistributionLocationForm);

export default compose(withStyles(formStyle))(SubDistributionLocationForm);
