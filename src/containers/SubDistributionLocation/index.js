import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
// import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateSubDistributionLocation from "./SubDistributionLocationCreate";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  ADD_NEW_SUBDISTRIBUTIONLOCATION,
  UPDATE_SUBDISTRIBUTIONLOCATION,
  DELETE_SUBDISTRIBUTIONLOCATION,
  FETCH_SUBDISTRIBUTIONLOCATION,
  FETCH_DISTRIBUTIONLOCATION
} from "../../actions/actionConstants";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  editSubDistributionLocation,
  clearSubDistributionLocation
} from "../../actions/admin";

class SubDistributionLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const {
      fetchSubDistributionLocation,
      fetchDistributionLocation
    } = this.props;
    fetchSubDistributionLocation();
    fetchDistributionLocation();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    const {
      createSubDistributionLocation,
      updateSubDistributionLocation
    } = this.props;
    const updatedvalue = {
      distribution_location_id: value.distributionLocation.id,
      id: value.id,
      name: value.name
    };
    if (value.id) {
      console.log(
        "SubDistributionLocaion container => updateSubDistributionLocation()",
        updatedvalue
      );
      updateSubDistributionLocation(updatedvalue);
    } else {
      createSubDistributionLocation(updatedvalue);
    }

    this.setState({ openForm: false });
  };
  handleEdit = rowData => {
    console.log(
      "SubDistribution Location Container => index => handleEdit()",
      rowData.original
    );
    const { editSubDistributionLocationDetails } = this.props;
    editSubDistributionLocationDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    console.log(
      "SubDistribution Location Container => index => handleDete()",
      rowData
    );
    const { deleteSubDistributionLocationDetails } = this.props;
    deleteSubDistributionLocationDetails(rowData.original);
  };
  render() {
    const {
      subDistributionLocationList,
      distributionLocationList,
      classes
    } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = subDistributionLocationList.length
      ? subDistributionLocationList.length
      : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createSubDistributionLocation}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.subDistributionLocationContainor}>
          <ReactTable
            data={subDistributionLocationList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 5
              },
              {
                Header: "Name",
                className: classes.eachRow,
                accessor: "name"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${
              classes.subDistributionLocationTable
              }`}
          />
        </div>

        <CreateSubDistributionLocation
          openForm={openForm}
          distributionLocationList={distributionLocationList}
          closeForm={this.closeForm}
          initialValue={initialValue}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

SubDistributionLocation.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  subDistributionLocationList: PropTypes.array.isRequired,
  distributionLocationList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  subDistributionLocationList:
    state.subDistributionLocationCRUD.subDistributionLocationList,
  distributionLocationList:
    state.distributionLocationCRUD.distributionLocationList
});
const mapDispatchToProps = dispatch => ({
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  fetchDistributionLocation: () => {
    dispatch(action(FETCH_DISTRIBUTIONLOCATION));
  },
  createSubDistributionLocation: value => {
    dispatch(action(ADD_NEW_SUBDISTRIBUTIONLOCATION, value));
  },
  updateSubDistributionLocation: value => {
    dispatch(action(UPDATE_SUBDISTRIBUTIONLOCATION, value));
  },
  editSubDistributionLocationDetails: value => {
    dispatch(editSubDistributionLocation(value));
  },
  clearSubDistributionLocation: () => {
    dispatch(clearSubDistributionLocation());
  },
  deleteSubDistributionLocationDetails: value => {
    dispatch(action(DELETE_SUBDISTRIBUTIONLOCATION, value));
  }
});
const SubDistributionLocationCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(SubDistributionLocation);

export default compose(withStyles(styles))(SubDistributionLocationCmpt);
