import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { withStyles } from "@material-ui/core/styles";

import { compose } from "redux";
import { connect } from "react-redux";

import styles from "./styles/style";

const Loader = ({ classes, progress }) => {
  return null;
  // return (
  //   progress && (
  //     <React.Fragment>
  //       <LinearProgress
  //         classes={{
  //           colorPrimary: classes.progressPrimary,
  //           barColorPrimary: classes.barColorPrimary,
  //           root: classes.progressRoot,
  //         }}
  //       />
  //       <div className={classes.loaderMask} />
  //     </React.Fragment>
  //   )
  // );
};

// const MapStateToProps = ({ loader: { progress } }) => {
//   return {
//     progress,
//   };
// };

export default compose(
  // connect(MapStateToProps),
  withStyles(styles),
)(Loader);
