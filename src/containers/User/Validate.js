const validate = values => {
  const errors = {};

  if (!values.name) {
    errors.name = "Required";
  }
  if (!values.username) {
    errors.username = "Required";
  } else if (values.username.length < 10) {
    errors.username = "Invalid";
  }
  if (!values.password) {
    errors.password = "Required";
  }
  if (!values.confirmPassword) {
    errors.confirmPassword = "Required";
  } else if (values.confirmPassword !== values.password) {
    errors.confirmPassword = "Match error";
  }

  return errors;
};

export default validate;
