import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateUser from "././CreateUser";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  FETCH_FORM_COMPANY,
  FETCH_USER,
  ADD_NEW_USER,
  UPDATE_USER,
  DELETE_USER
} from "../../actions/actionConstants";
import { editUser, clearUser } from "../../actions/admin";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false
    };
  }
  componentDidMount() {
    const { fetchCompany, fetchUser } = this.props;
    fetchCompany();
    fetchUser();
  }

  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    const { clearUser } = this.props;
    this.setState({ openForm: true });
    clearUser();
  };
  submitAction = value => {
    const { createUser, updateUser } = this.props;
    console.log("user  submitted value", value);
    if (value.id) {
      updateUser(value);
    } else {
      createUser(value);
    }
    this.closeForm();
  };
  handleEdit = rowData => {
    const { editUserDetails } = this.props;
    editUserDetails(rowData.original);
    this.setState({ openForm: true });
  };
  handleDelete = rowData => {
    const { deleteUserDetails } = this.props;
    deleteUserDetails(rowData.original);
  };
  render() {
    const { companyList, userList, classes } = this.props;
    const { openForm } = this.state;
    const tableLength = userList.length ? userList.length : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createUser}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.companyContainor}>
          <ReactTable
            data={userList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    <DeleteIcon onClick={() => this.handleDelete(rowData)} />
                  </div>
                ),
                minWidth: 40
              },
              {
                Header: "Name",
                className: classes.eachRow,
                accessor: "user.name"
              },
              {
                Header: "Number",
                className: classes.eachRow,
                accessor: "user.username"
              },
              {
                Header: "Email",
                className: classes.eachRow,
                accessor: "user.email"
              },
              {
                Header: "Company",
                className: classes.eachRow,
                accessor: "company.name"
              },

              {
                Header: "Role",
                className: classes.eachRow,
                Cell: rowData => {
                  return (
                    <div>
                      {rowData.original.user.roles ? rowData.original.user.roles[0].name : ''}
                    </div>
                  );
                },
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.companyTable}`}
          />
        </div>

        <CreateUser
          openForm={openForm}
          companyList={companyList}
          closeForm={this.closeForm}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

User.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  companyList: PropTypes.array.isRequired,
  userList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  companyList: state.companyCRUD.companyFormList,
  userList: state.userCRUD.userList
});
const mapDispatchToProps = dispatch => ({
  fetchCompany: () => {
    dispatch(action(FETCH_FORM_COMPANY));
  },
  fetchUser: () => {
    dispatch(action(FETCH_USER));
  },
  createUser: value => {
    dispatch(action(ADD_NEW_USER, value));
  },
  updateUser: value => {
    dispatch(action(UPDATE_USER, value));
  },
  editUserDetails: value => {
    dispatch(editUser(value));
  },
  clearUser: () => {
    dispatch(clearUser());
  },
  deleteUserDetails: value => {
    dispatch(action(DELETE_USER, value));
  }
});
const UserCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(User);

export default compose(withStyles(styles))(UserCmpt);
