import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
// import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateStationLocation from "./StationLocationCreate";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  ADD_NEW_STATIONLOCATION,
  UPDATE_STATIONLOCATION,
  DELETE_STATIONLOCATION,
  FETCH_STATIONLOCATION,
  FETCH_SUBDISTRIBUTIONLOCATION
} from "../../actions/actionConstants";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { editStationLocation, clearStationLocation } from "../../actions/admin";

class StationLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const { fetchStationLocation, fetchSubDistributionLocation } = this.props;
    fetchStationLocation();
    fetchSubDistributionLocation();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    const { createStationLocation, updateStationLocation } = this.props;
    console.log(
      "StationLocaion container => before value.id submitAction",
      value
    );
    const updatedValue = {
      subdistributionLocation_id: value.subdistributionLocation.id,
      name: value.name,
      id: value.id
    };
    if (value.id) {
      console.log("StationLocaion container => updateStationLocation()", value);
      updateStationLocation(updatedValue);
    } else {
      createStationLocation(updatedValue);
    }

    this.setState({ openForm: false });
  };
  handleEdit = rowData => {
    console.log(
      "Station Location Container => index => handleEdit()",
      rowData.original
    );
    const { editStationLocationDetails } = this.props;
    editStationLocationDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    console.log("Station Location Container => index => handleDete()", rowData);
    const { deleteStationLocationDetails } = this.props;
    deleteStationLocationDetails(rowData.original);
  };
  render() {
    const {
      stationLocationList,
      subDistributionLocationList,
      classes
    } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = stationLocationList.length
      ? stationLocationList.length
      : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createStationLocation}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.stationLocationContainor}>
          <ReactTable
            data={stationLocationList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                  </div>
                ),
                minWidth: 5
              },
              {
                Header: "Name",
                className: classes.eachRow,
                accessor: "name"
              },
              {
                Header: "Subdistribution Location",
                className: classes.eachRow,
                accessor: "subdistributionLocation.name"
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${classes.stationLocationTable}`}
          />
        </div>

        <CreateStationLocation
          openForm={openForm}
          subDistributionLocationList={subDistributionLocationList}
          closeForm={this.closeForm}
          initialValue={initialValue}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

StationLocation.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  stationLocationList: PropTypes.array.isRequired,
  subDistributionLocationList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  stationLocationList: state.stationLocationCRUD.stationLocationList,
  subDistributionLocationList:
    state.subDistributionLocationCRUD.subDistributionLocationList
});
const mapDispatchToProps = dispatch => ({
  fetchStationLocation: () => {
    dispatch(action(FETCH_STATIONLOCATION));
  },
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  createStationLocation: value => {
    dispatch(action(ADD_NEW_STATIONLOCATION, value));
  },
  updateStationLocation: value => {
    dispatch(action(UPDATE_STATIONLOCATION, value));
  },
  editStationLocationDetails: value => {
    dispatch(editStationLocation(value));
  },
  clearStationLocation: () => {
    dispatch(clearStationLocation());
  },
  deleteStationLocationDetails: value => {
    dispatch(action(DELETE_STATIONLOCATION, value));
  }
});
const StationLocationCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(StationLocation);

export default compose(withStyles(styles))(StationLocationCmpt);
