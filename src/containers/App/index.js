/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";

// import Sidebar from "containers/Sidebar";
import Sidebar from "../Sidebar";

// import header from "containers/Header";
import Header from "../Header";

import Auth from "../../components/Auth";

import Router from "../../components/Router";
import { approutes, authroutes } from "../../routes";
// import Header from "../../components/Header";
import Footer from "../../components/Footer";

import styles from "./styles/style";
import "./styles/commonStyles.css";

const App = props => {
  const { classes } = props;
  return (
    <Fragment>
      <CssBaseline />
      {
        Auth.isAuthenticated() ?
        <div className={classes.container}>
          <Header />
          <Sidebar />
          <div className={classes.app_wrapper}>
            <Router routes={approutes} />
          </div>
          {/* <Footer /> */}
        </div> :
        <div className={classes.container}>
          <div className={classes.bodyContainer}>
            <Router routes={authroutes} />
          </div>
        </div>
      }
    </Fragment>
  );
};

App.propTypes = {
  classes: PropTypes.object.isRequired
};

const MapStateToProps = state => ({
  data: state.userInfo,
  language: state.language
});
export default compose(
  withRouter,
  connect(MapStateToProps),
  withStyles(styles)
)(App);
