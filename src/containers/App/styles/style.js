// import gothamMedium from "../assets/fonts/GothamRoundedMedium_21022.ttf";
// import gothamBold from "../assets/fonts/GothamRounded-Bold.otf";
// import gothamBook from "../assets/fonts/GothamRounded-Book.otf";

const styles = theme => ({
  // "@font-face": [
  //   {
  //     fontFamily: "Gotham-Medium",
  //     src: `url(${gothamMedium}) format("truetype")`,
  //   },
  //   {
  //     fontFamily: "Gotham-Bold",
  //     src: `url(${gothamBold}) format("opentype")`,
  //   },
  //   {
  //     fontFamily: "Gotham-Book",
  //     src: `url(${gothamBook}) format("opentype")`,
  //   },
  // ],
  // a: {
  //   color: "red",
  // },
  // "@global": {
  //   body: {
  //     fontFamily: "Gotham-Book, Arial",
  //   },
  //   a: {
  //     color: "#19ACE3",
  //     textDecoration: "none",
  //   },
  // },
  container: {
    height: '100%',
    flex: 1,
    display: 'flex'
  },
  app_wrapper: {
    display: 'flex',
    width: '100%',
    height: 'calc(100% - 100px)',
    boxSizing: 'border-box',
    marginTop: '50px'
  },
  bodyContainer: {
    height: '100%',
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.palette.primary.main
  }
});

export default styles;
