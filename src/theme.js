import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: '#29484e',
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: '#00BB30',
      // dark: will be calculated from palette.secondary.main,
      contrastText: '#fff',
    },
    light: '#fff',
    dark: '#000'
    // error: will use the default color
  },
});

export default theme;