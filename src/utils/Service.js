import axios from "axios";

const instance = axios.create({
  baseURL: "https://stormy-beach-62291.herokuapp.com/api"
});
const mockable_instance = axios.create({
  baseURL: "http://demo4242644.mockable.io/"
});
const mockable_instance2 = axios.create({
  baseURL: "https://demo6055019.mockable.io/ "
});
export default instance;
export { mockable_instance, mockable_instance2 };
