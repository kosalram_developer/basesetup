import { getActionTypes } from "redux-axios-middleware";
import { emptyFunction } from "./util";

export const isAxiosRequest = action =>
  action.payload && action.payload.request;

export const onSuccess = (
  { action, next, response, getState, dispatch },
  options,
) => {
  const nextAction = {
    type: getActionTypes(action, options)[1],
    payload: response,
    meta: {
      previousAction: action,
    },
  };
  const { onSuccessCb = emptyFunction } = options;
  onSuccessCb({ action, response, getState, dispatch }, options);
  next(nextAction);
  return nextAction;
};

export const onError = (
  { action, next, error, getState, dispatch },
  options,
) => {
  let errorObject;
  if (!error.response) {
    errorObject = {
      data: error.message,
      status: 0,
    };
    if (process.env.NODE_ENV !== "production") {
      console.log("HTTP Failure in Axios", error);
    }
  } else {
    errorObject = error;
  }
  const nextAction = {
    type: getActionTypes(action, options)[2],
    error: errorObject,
    meta: {
      previousAction: action,
    },
  };
  const { onErrorCb = emptyFunction } = options;
  onErrorCb({ action, error, getState, dispatch }, options);
  next(nextAction);
  return nextAction;
};

const axiosMiddlewareWithCallback = store => next => action => {
  let newAction = action;
  if (isAxiosRequest(action)) {
    const { payload } = action;
    const { options = {} } = payload;
    const { onSuccess: onSuccessCb, onError: onErrorCb } = options;
    newAction = {
      ...action,
      payload: {
        ...payload,
        options: {
          ...options,
          onSuccess,
          onError,
          onSuccessCb,
          onErrorCb,
        },
      },
    };
  }

  let result = next(newAction);
  return result;
};

export default axiosMiddlewareWithCallback;
