import {
    LOGIN_SUCCEEDED, LOGIN_REQUEST, LOGIN_FAIL,
  } from './actionConstants';

export const loginRequest = data => ({
  type: LOGIN_REQUEST,
  data
});

export const loginSuccess = data => ({
  type: LOGIN_SUCCEEDED,
  data
});

export const loginFail = error => ({
  type: LOGIN_FAIL,
  error
});