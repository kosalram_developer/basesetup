import {
  FETCH_COMPANY_SUCCEEDED,
  ADD_NEW_COMPANY_SUCCEEDED,
  FETCH_COMPANY,
  UPDATE_COMPANY_SUCCEEDED,
  FETCH_COMPANY_FORM_SUCCEEDED,
  DELETE_COMPANY_SUCCEEDED,
  EDIT_COMPANY,
  CLEAR_COMPANY,
  ADD_NEW_THEATRE_SUCCEEDED,
  FETCH_THEATRE,
  DELETE_THEATRE_SUCCEEDED,
  UPDATE_THEATRE_SUCCEEDED,
  FETCH_THEATRE_SUCCEEDED,
  EDIT_THEATRE,
  CLEAR_THEATRE,
  ADD_NEW_SUBDISTRIBUTION_SUCCEEDED,
  DELETE_SUBDISTRIBUTION_SUCCEEDED,
  UPDATE_SUBDISTRIBUTION_SUCCEEDED,
  FETCH_SUBDISTRIBUTION_SUCCEEDED,
  EDIT_SUBDISTRIBUTION,
  CLEAR_SUBDISTRIBUTION,
  ADD_NEW_DISTRIBUTION_SUCCEEDED,
  DELETE_DISTRIBUTION_SUCCEEDED,
  UPDATE_DISTRIBUTION_SUCCEEDED,
  FETCH_DISTRIBUTION_SUCCEEDED,
  EDIT_DISTRIBUTION,
  CLEAR_DISTRIBUTION,
  ADD_NEW_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  DELETE_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  UPDATE_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  FETCH_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  FETCH_SUBDISTRIBUTIONLOCATION,
  EDIT_SUBDISTRIBUTIONLOCATION,
  CLEAR_SUBDISTRIBUTIONLOCATION,
  FETCH_DISTRIBUTIONLOCATION,
  DELETE_DISTRIBUTIONLOCATION_SUCCEEDED,
  FETCH_DISTRIBUTIONLOCATION_SUCCEEDED,
  EDIT_DISTRIBUTIONLOCATION,
  CLEAR_DISTRIBUTIONLOCATION,
  ADD_NEW_MOVIE_SUCCEEDED,
  DELETE_MOVIE_SUCCEEDED,
  UPDATE_MOVIE_SUCCEEDED,
  FETCH_MOVIE_SUCCEEDED,
  EDIT_MOVIE,
  CLEAR_MOVIE,
  ADD_NEW_USER_SUCCEEDED,
  DELETE_USER_SUCCEEDED,
  UPDATE_USER_SUCCEEDED,
  FETCH_USER_SUCCEEDED,
  EDIT_USER,
  CLEAR_USER,
  ADD_NEW_CALCULATION_SUCCEEDED,
  DELETE_CALCULATION_SUCCEEDED,
  UPDATE_CALCULATION_SUCCEEDED,
  FETCH_CALCULATION_SUCCEEDED,
  EDIT_CALCULATION,
  CLEAR_CALCULATION,
  FETCH_THEATRE_BY_MOVIE_SUCCESS,
  FETCH_COLLECTION,
  ADD_NEW_COLLECTION_SUCCEEDED,
  DELETE_COLLECTION_SUCCEEDED,
  UPDATE_COLLECTION_SUCCEEDED,
  FETCH_COLLECTION_SUCCEEDED,
  EDIT_COLLECTION,
  CLEAR_COLLECTION,
  ADD_NEW_SCHEDULE_SUCCEEDED,
  FETCH_SCHEDULE,
  DELETE_SCHEDULE_SUCCEEDED,
  UPDATE_SCHEDULE_SUCCEEDED,
  FETCH_SCHEDULE_SUCCEEDED,
  EDIT_SCHEDULE,
  CLEAR_SCHEDULE,
  ADD_NEW_AGREEMENT_SUCCEEDED,
  FETCH_AGREEMENT,
  DELETE_AGREEMENT_SUCCEEDED,
  UPDATE_AGREEMENT_SUCCEEDED,
  FETCH_AGREEMENT_SUCCEEDED,
  EDIT_AGREEMENT,
  CLEAR_AGREEMENT,
  FETCH_STATIONLOCATION_SUCCEEDED,
  FETCH_STATIONLOCATION,
  ADD_NEW_STATIONLOCATION_SUCCEEDED,
  UPDATE_STATIONLOCATION_SUCCEEDED,
  DELETE_STATIONLOCATION_SUCCEEDED,
  EDIT_STATIONLOCATION,
  CLEAR_STATIONLOCATION,
  FETCH_ASSIGN_SUCCEEDED,
  FETCH_ASSIGN,
  ADD_NEW_ASSIGN_SUCCEEDED,
  UPDATE_ASSIGN_SUCCEEDED,
  DELETE_ASSIGN_SUCCEEDED,
  EDIT_ASSIGN,
  CLEAR_ASSIGN
} from "./actionConstants";

// Movie
export const fetchMovieSuccess = data => ({
  type: FETCH_MOVIE_SUCCEEDED,
  data
});
export const createMovieSuccess = data => ({
  type: ADD_NEW_MOVIE_SUCCEEDED,
  data
});
export const updateMovieSuccess = data => ({
  type: UPDATE_MOVIE_SUCCEEDED,
  data
});
export const deleteMovieSuccess = data => ({
  type: DELETE_MOVIE_SUCCEEDED,
  data
});
export const editMovie = data => ({
  type: EDIT_MOVIE,
  data
});
export const clearMovie = () => ({
  type: CLEAR_MOVIE
});

// User
export const fetchUserSuccess = data => ({
  type: FETCH_USER_SUCCEEDED,
  data
});
export const createUserSuccess = data => ({
  type: ADD_NEW_USER_SUCCEEDED,
  data
});
export const updateUserSuccess = data => ({
  type: UPDATE_USER_SUCCEEDED,
  data
});
export const deleteUserSuccess = data => ({
  type: DELETE_USER_SUCCEEDED,
  data
});
export const editUser = data => ({
  type: EDIT_USER,
  data
});
export const clearUser = () => ({
  type: CLEAR_USER
});

// Company
export const fetchCompanySuccess = data => ({
  type: FETCH_COMPANY_SUCCEEDED,
  data
});
export const fetchformcompanySuccess = data => ({
  type: FETCH_COMPANY_FORM_SUCCEEDED,
  data
});

export const createCompanySuccess = data => ({
  type: FETCH_COMPANY,
  data
});
export const updateCompanySuccess = data => ({
  type: FETCH_COMPANY,
  data
});
export const deleteCompanySuccess = data => ({
  type: DELETE_COMPANY_SUCCEEDED,
  data
});
export const editCompany = data => ({
  type: EDIT_COMPANY,
  data
});
export const clearCompany = () => ({
  type: CLEAR_COMPANY
});

// Distribution Location
export const fetchDistributionLocationSuccess = data => ({
  type: FETCH_DISTRIBUTIONLOCATION_SUCCEEDED,
  data
});

export const createDistributionLocationSuccess = data => ({
  type: FETCH_DISTRIBUTIONLOCATION,
  data
});

export const updateDistributionLocationSuccess = data => ({
  type: FETCH_DISTRIBUTIONLOCATION,
  data
});
export const deleteDistributionLocationSuccess = data => ({
  type: DELETE_DISTRIBUTIONLOCATION_SUCCEEDED,
  data
});
export const editDistributionLocation = data => ({
  type: EDIT_DISTRIBUTIONLOCATION,
  data
});
export const clearDistributionLocation = () => ({
  type: CLEAR_DISTRIBUTIONLOCATION
});

// SubDistribution Location
export const fetchSubDistributionLocationSuccess = data => ({
  type: FETCH_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  data
});

export const createSubDistributionLocationSuccess = data => ({
  type: FETCH_SUBDISTRIBUTIONLOCATION,
  data
});

export const updateSubDistributionLocationSuccess = data => ({
  type: FETCH_SUBDISTRIBUTIONLOCATION,
  data
});
export const deleteSubDistributionLocationSuccess = data => ({
  type: DELETE_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  data
});
export const editSubDistributionLocation = data => ({
  type: EDIT_SUBDISTRIBUTIONLOCATION,
  data
});
export const clearSubDistributionLocation = () => ({
  type: CLEAR_SUBDISTRIBUTIONLOCATION
});

// Distribution Location
export const fetchStationLocationSuccess = data => ({
  type: FETCH_STATIONLOCATION_SUCCEEDED,
  data
});

export const createStationLocationSuccess = data => ({
  type: FETCH_STATIONLOCATION,
  data
});

export const updateStationLocationSuccess = data => ({
  type: FETCH_STATIONLOCATION,
  data
});
export const deleteStationLocationSuccess = data => ({
  type: DELETE_STATIONLOCATION_SUCCEEDED,
  data
});
export const editStationLocation = data => ({
  type: EDIT_STATIONLOCATION,
  data
});
export const clearStationLocation = () => ({
  type: CLEAR_STATIONLOCATION
});

// Distribution
export const fetchDistributionSuccess = data => ({
  type: FETCH_DISTRIBUTION_SUCCEEDED,
  data
});

export const createDistributionSuccess = data => ({
  type: ADD_NEW_DISTRIBUTION_SUCCEEDED,
  data
});

export const updateDistributionSuccess = data => ({
  type: UPDATE_DISTRIBUTION_SUCCEEDED,
  data
});
export const deleteDistributionSuccess = data => ({
  type: DELETE_DISTRIBUTION_SUCCEEDED,
  data
});
export const editDistribution = data => ({
  type: EDIT_DISTRIBUTION,
  data
});
export const clearDistribution = () => ({
  type: CLEAR_DISTRIBUTION
});

// SubDistribution
export const fetchSubDistributionSuccess = data => ({
  type: FETCH_SUBDISTRIBUTION_SUCCEEDED,
  data
});

export const createSubDistributionSuccess = data => ({
  type: ADD_NEW_SUBDISTRIBUTION_SUCCEEDED,
  data
});

export const updateSubDistributionSuccess = data => ({
  type: UPDATE_SUBDISTRIBUTION_SUCCEEDED,
  data
});
export const deleteSubDistributionSuccess = data => ({
  type: DELETE_SUBDISTRIBUTION_SUCCEEDED,
  data
});
export const editSubDistribution = data => ({
  type: EDIT_SUBDISTRIBUTION,
  data
});
export const clearSubDistribution = () => ({
  type: CLEAR_SUBDISTRIBUTION
});

// Theatre
export const fetchTheatreSuccess = data => ({
  type: FETCH_THEATRE_SUCCEEDED,
  data
});

export const createTheatreSuccess = data => ({
  type: FETCH_THEATRE,
  data
});
export const updateTheatreSuccess = data => ({
  type: UPDATE_THEATRE_SUCCEEDED,
  data
});
export const deleteTheatreSuccess = data => ({
  type: DELETE_THEATRE_SUCCEEDED,
  data
});
export const editTheatre = data => ({
  type: EDIT_THEATRE,
  data
});
export const clearTheatre = () => ({
  type: CLEAR_THEATRE
});

// Calculation
export const fetchCalculationSuccess = data => ({
  type: FETCH_CALCULATION_SUCCEEDED,
  data
});

export const createCalculationSuccess = data => ({
  type: ADD_NEW_CALCULATION_SUCCEEDED,
  data
});
export const updateCalculationSuccess = data => ({
  type: UPDATE_CALCULATION_SUCCEEDED,
  data
});
export const deleteCalculationSuccess = data => ({
  type: DELETE_CALCULATION_SUCCEEDED,
  data
});
export const editCalculation = data => ({
  type: EDIT_CALCULATION,
  data
});
export const clearCalculation = () => ({
  type: CLEAR_CALCULATION
});

// Collection
export const fetchtheatrebymovieSuccess = data => ({
  type: FETCH_THEATRE_BY_MOVIE_SUCCESS,
  data
});
export const fetchCollectionSuccess = data => ({
  type: FETCH_COLLECTION_SUCCEEDED,
  data
});

export const createCollectionSuccess = data => ({
  type: FETCH_COLLECTION,
  data
});
export const updateCollectionSuccess = data => ({
  type: UPDATE_COLLECTION_SUCCEEDED,
  data
});
export const deleteCollectionSuccess = data => ({
  type: DELETE_COLLECTION_SUCCEEDED,
  data
});
export const editCollection = data => ({
  type: EDIT_COLLECTION,
  data
});
export const clearCollection = () => ({
  type: CLEAR_COLLECTION
});

// Schedule
export const fetchScheduleSuccess = data => ({
  type: FETCH_SCHEDULE_SUCCEEDED,
  data
});

export const createScheduleSuccess = data => ({
  type: FETCH_SCHEDULE,
  data
});
export const updateScheduleSuccess = data => ({
  type: UPDATE_SCHEDULE_SUCCEEDED,
  data
});
export const deleteScheduleSuccess = data => ({
  type: DELETE_SCHEDULE_SUCCEEDED,
  data
});
export const editSchedule = data => ({
  type: EDIT_SCHEDULE,
  data
});
export const clearSchedule = () => ({
  type: CLEAR_SCHEDULE
});

// Agreement
export const fetchAgreementSuccess = data => ({
  type: FETCH_AGREEMENT_SUCCEEDED,
  data
});

export const createAgreementSuccess = data => ({
  type: FETCH_AGREEMENT,
  data
});
export const updateAgreementSuccess = data => ({
  type: FETCH_AGREEMENT,
  data
});
export const deleteAgreementSuccess = data => ({
  type: DELETE_AGREEMENT_SUCCEEDED,
  data
});
export const editAgreement = data => ({
  type: EDIT_AGREEMENT,
  data
});
export const clearAgreement = () => ({
  type: CLEAR_AGREEMENT
});

// Movie Assign
export const fetchAssignSuccess = data => ({
  type: FETCH_ASSIGN_SUCCEEDED,
  data
});

export const createAssignSuccess = data => ({
  type: FETCH_ASSIGN,
  data
});
export const updateAssignSuccess = data => ({
  type: FETCH_ASSIGN,
  data
});
export const deleteAssignSuccess = data => ({
  type: DELETE_ASSIGN_SUCCEEDED,
  data
});
export const editAssign = data => ({
  type: EDIT_ASSIGN,
  data
});
export const clearAssign = () => ({
  type: CLEAR_ASSIGN
});
