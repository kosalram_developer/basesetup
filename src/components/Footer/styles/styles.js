const styles = theme => ({
  footer: {
    position: "fixed",
    width: "100%",
    bottom: 0
  }
});

export default styles;
