import React from "react";
import { Route, Redirect } from "react-router-dom";
import Auth from "./Auth";

const PrivateRoute = ({ component: Component, isRedirect, ...rest }) => {
  let canRedirect = true;
  let redirectTo = "/login";

  if (isRedirect) {
    redirectTo = "/";
    canRedirect = false;
  } else {
    canRedirect = Auth.isAuthenticated();
  }

  return (
    <Route
      {...rest}
      render={props =>
        canRedirect === true ? (
          <Component {...props} />
        ) : (
          <Redirect to={redirectTo} />
        )
      }
    />
  );
};

export default PrivateRoute;
