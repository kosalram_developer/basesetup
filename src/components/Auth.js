import instance from "../utils/Service";

const Auth = {
  isAuthenticated() {
    const isAuthenticated = !!sessionStorage.access_token;
    if (isAuthenticated) this.setAxiosHeader();
    return isAuthenticated;
  },
  setAxiosHeader() {
    const { access_token } = sessionStorage;
    instance.defaults.headers["Authorization"] = access_token;
  },
  authenticate() {},
  setSessionStorage(access_token) {
    sessionStorage.access_token = access_token;
  },
  signout() {
    sessionStorage.clear();
  }
};

export default Auth;
