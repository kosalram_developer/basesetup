import {
  FETCH_ASSIGN_SUCCEEDED,
  ADD_NEW_ASSIGN_SUCCEEDED,
  UPDATE_ASSIGN_SUCCEEDED,
  EDIT_ASSIGN,
  CLEAR_ASSIGN,
  DELETE_ASSIGN_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  formValues: [],
  assignList: [],
  assignEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  Assign -------------------------------

    case FETCH_ASSIGN_SUCCEEDED:
      return {
        ...state,
        assignList: action.data
      };
    case ADD_NEW_ASSIGN_SUCCEEDED:
      const assignList = state.assignList;
      assignList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        assignList
      };
    case UPDATE_ASSIGN_SUCCEEDED:
      const assignobjIndex = state.assignList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const assignupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const updatedassignList = [
        ...state.assignList.slice(0, assignobjIndex),
        assignupdatedObj,
        ...state.assignList.slice(assignobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        assignList: updatedassignList
      };
    case EDIT_ASSIGN:
      console.log("----------EDIT_COMPANY------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        assignEdit: action.data
      };
    case CLEAR_ASSIGN:
      return {
        ...state,
        assignEdit: {}
      };
    case DELETE_ASSIGN_SUCCEEDED:
      const assignindex = state.scheduleList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const assignrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeleteassignList = [
        ...state.assignList.slice(0, assignindex),
        assignrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        assignList: afterDeleteassignList
      };
    default:
      return state;
  }
}
