import {
  FETCH_THEATRE_BY_MOVIE_SUCCESS,
  DELETE_COLLECTION_SUCCEEDED,
  CLEAR_COLLECTION,
  EDIT_COLLECTION,
  UPDATE_COLLECTION_SUCCEEDED,
  ADD_NEW_COLLECTION_SUCCEEDED,
  FETCH_COLLECTION_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  collectionList: [],
  theatreList: [],
  formValues: [],
  collectionEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  collection -------------------------------

    case FETCH_COLLECTION_SUCCEEDED:
      return {
        ...state,
        collectionList: action.data,
        collectionEdit: {}
      };
    case FETCH_THEATRE_BY_MOVIE_SUCCESS:
      return {
        ...state,
        theatreList: action.data
      };
    case ADD_NEW_COLLECTION_SUCCEEDED:
      const collectionList = state.collectionList;
      collectionList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        collectionList
      };
    case UPDATE_COLLECTION_SUCCEEDED:
      const collectionobjIndex = state.collectionList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const collectionupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const updatedcollectionList = [
        ...state.collectionList.slice(0, collectionobjIndex),
        collectionupdatedObj,
        ...state.collectionList.slice(collectionobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        collectionList: updatedcollectionList
      };
    case EDIT_COLLECTION:
      console.log("action.data-----------------------------------------");
      console.log(action.data);

      let mydate = new Date(action.data.date);
      mydate.setHours(action.data.slotTime.split(":")[0]);
      mydate.setMinutes(action.data.slotTime.split(":")[1]);
      console.log(mydate);
      let collection = {
        ...action.data,
        slotTime: mydate
      };
      return {
        ...state,
        collectionEdit: collection
      };
    case CLEAR_COLLECTION:
      return {
        ...state,
        collectionEdit: {}
      };
    case DELETE_COLLECTION_SUCCEEDED:
      const collectionindex = state.collectionList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const collectionrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletecollectionList = [
        ...state.collectionList.slice(0, collectionindex),
        collectionrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        collectionList: afterDeletecollectionList
      };
    default:
      return state;
  }
}
