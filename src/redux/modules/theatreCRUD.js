import {
  ADD_NEW_THEATRE_SUCCEEDED,
  DELETE_THEATRE_SUCCEEDED,
  UPDATE_THEATRE_SUCCEEDED,
  FETCH_THEATRE_SUCCEEDED,
  EDIT_THEATRE,
  CLEAR_THEATRE
} from "../../actions/actionConstants";

const initialState = {
  theatreList: [],
  theatreEdit: {},
  formValues: [],
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    //----------------------- Theatre --------------------------------------

    case FETCH_THEATRE_SUCCEEDED:
      return {
        ...state,
        theatreList: action.data
      };
    case ADD_NEW_THEATRE_SUCCEEDED:
      const theatreList = state.theatreList;
      theatreList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        theatreList
      };
    case UPDATE_THEATRE_SUCCEEDED:
      const theatreListobjIndex = state.theatreList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const theatreListupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const theatreListupdatedTheatreList = [
        ...state.theatreList.slice(0, theatreListobjIndex),
        theatreListupdatedObj,
        ...state.theatreList.slice(theatreListobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        theatreList: theatreListupdatedTheatreList
      };
    case EDIT_THEATRE:
      let item = action.data;
      item.distribution = {
        value: item.distribution_location_id
      };
      console.log("EDIT_THEATRE=====");
      console.log("EDIT_THEATRE", item);

      return {
        ...state,
        theatreEdit: action.data
      };
    case CLEAR_THEATRE:
      return {
        ...state,
        theatreEdit: {}
      };
    case DELETE_THEATRE_SUCCEEDED:
      const theatreListindex = state.theatreList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const theatreListrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const theatreListafterDeleteTheatreList = [
        ...state.companyList.slice(0, theatreListindex),
        theatreListrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        theatreList: theatreListafterDeleteTheatreList
      };
    default:
      return state;
  }
}
