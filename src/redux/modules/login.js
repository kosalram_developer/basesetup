import {
  LOGIN_REQUEST,
  LOGIN_SUCCEEDED,
  LOGIN_FAIL,
  LOGOUT_SUCCEEDED
} from "../../actions/actionConstants";
import Auth from "../../components/Auth";
const initialState = {
  user: {},
  isLogIn: false,
  loading: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true
      };
    case LOGIN_SUCCEEDED:
      Auth.setSessionStorage(action.data.id);
      return {
        ...state,
        user: action.data,
        isLogIn: true,
        loading: false
      };
    case LOGIN_FAIL:
      return {
        ...state,
        loading: false
      };
    case LOGOUT_SUCCEEDED:
      return {
        ...state,
        isLogIn: false
      };

    default:
      return state;
  }
}
