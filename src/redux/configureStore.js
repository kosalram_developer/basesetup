/**
 * Create the store with dynamic reducers
 */
import React from "react";
import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware, connectRouter } from "connected-react-router";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import axiosMiddleware from "redux-axios-middleware";
import createSagaMiddleware from "redux-saga";
import createReducer from "./reducers";
// import axiosMiddleWareWithCallback from "./utils/axiosMiddlewareWithCallback";
import rootSaga from "../sagas/sagas";
import client from "../utils/Service";
// import { showError } from "./containers/Notification/reducer";
// import { showLoader, hideLoader } from "./containers/PageLoader/reducer";

const sagaMiddleware = createSagaMiddleware();
export default function configureStore(initialState = {}, history) {
  const middleware = routerMiddleware(history);

  const composeEnhancer =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const axiosInstance = client;
  const axiosMiddleWare = axiosMiddleware(axiosInstance);
  const store = createStore(
    createReducer(history),
    initialState,
    composeEnhancer(applyMiddleware(middleware, createLogger(), sagaMiddleware))
  );

  let pendingResponse = 0;

  const shouldShowLoader = axiosConfig => {
    const {
      reduxSourceAction: { payload: { showLoader = true } = {} } = {}
    } = axiosConfig;
    return showLoader;
  };

  axiosInstance.interceptors.request.use(config => {
    if (shouldShowLoader(config)) {
      pendingResponse++;
      // store.dispatch(showLoader());
    }
    return config;
  });

  axiosInstance.interceptors.response.use(
    response => {
      const { config } = response;
      if (shouldShowLoader(config)) {
        pendingResponse--;
        if (pendingResponse <= 0) {
          // store.dispatch(hideLoader());
        }
      }
      return response;
    },
    error => {
      const { message, response = {}, config } = error;
      const { data: { serviceerror: { subErrors = [] } = {} } = {} } = response;

      if (shouldShowLoader(config)) {
        pendingResponse--;
        if (pendingResponse <= 0) {
          // store.dispatch(hideLoader());
        }
      }

      let errorMessage = message;
      if (subErrors.length > 0) {
        const subErrorsArray = subErrors.map(({ message }, key) => (
          <p key={key}>{message}</p>
        ));
        errorMessage = subErrorsArray;
      }
      // store.dispatch(showError("", errorMessage));

      return Promise.reject(error);
    }
  );
  sagaMiddleware.run(rootSaga);
  if (process.env.NODE_ENV !== "production") {
    if (module.hot) {
      module.hot.accept("./reducers", () => {
        store.replaceReducer(connectRouter(history)(createReducer));
      });
    }
  }

  return store;
}
