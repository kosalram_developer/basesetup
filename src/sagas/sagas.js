import { all } from "redux-saga/effects";
import { adminSagas } from "./admin";
import { companyCRUD } from "./companyCRUD";
import { collectionCRUD } from "./collectionCRUD";
import { userCRUD } from "./userCRUD";
import { movieCRUD } from "./movieCRUD";
import { theatreCRUD } from "./theatreCRUD";
import { distributionLocationCRUD } from "./distributionLocationCRUD";
import { subDistributionLocationCRUD } from "./subDistributionLocationCRUD";
import { stationLocationCRUD } from "./stationLocationCRUD";
import { assignCRUD } from "./assignCRUD";
import { scheduleCRUD } from "./scheduleCRUD";
import { theatreAgreementCRUD } from "./theatreAgreementCRUD";

import { loginSagas } from "./login";

export default function* rootSaga() {
  yield all([
    ...adminSagas,
    ...loginSagas,
    ...companyCRUD,
    ...collectionCRUD,
    ...assignCRUD,
    ...userCRUD,
    ...movieCRUD,
    ...theatreCRUD,
    ...distributionLocationCRUD,
    ...stationLocationCRUD,
    ...subDistributionLocationCRUD,
    ...scheduleCRUD,
    ...theatreAgreementCRUD
  ]);
}
