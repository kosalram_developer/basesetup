import { put, call, takeLatest } from "redux-saga/effects";
import {
  FETCH_THEATRE_BY_MOVIE,
  FETCH_COLLECTION,
  ADD_NEW_COLLECTION,
  UPDATE_COLLECTION,
  DELETE_COLLECTION
} from "../actions/actionConstants";
import Api from "../api/collectionCRUD";
import {
  fetchtheatrebymovieSuccess,
  deleteCollectionSuccess,
  fetchCollectionSuccess,
  updateCollectionSuccess,
  createCollectionSuccess,
  clearCollection
} from "../actions/admin";

// Collection
export function* createCollection(value) {
  try {
    console.log("Create Collection Saga Called");
    console.log(value);
    const response = yield call(Api.createCollection, value);
    const param = {};
    param.movieId = value.data.movie_id;
    yield put(createCollectionSuccess(param));
  } catch (error) {
    console.log(error);
  }
}

export function* fetchtheatrebymovie(value) {
  try {
    console.log("fetchtheatrebymovie Saga Called");
    const response = yield call(Api.fetchtheatrebymovie, value);
    yield put(fetchtheatrebymovieSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateCollection(value) {
  try {
    console.log("update Collection Saga Called");
    const response = yield call(Api.updatecollection, value);
    yield put(updateCollectionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteCollection(value) {
  try {
    console.log("delete Collection Saga Called");
    yield call(Api.deletecollection, value);
    yield put(deleteCollectionSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchCollection(value) {
  try {
    console.log("fetch Collection saga called");
    console.log(value);

    const response = yield call(Api.fetchcollection, value);
    yield put(fetchCollectionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const collectionCRUD = [
  takeLatest(FETCH_THEATRE_BY_MOVIE, fetchtheatrebymovie),
  takeLatest(ADD_NEW_COLLECTION, createCollection),
  takeLatest(FETCH_COLLECTION, fetchCollection),
  takeLatest(UPDATE_COLLECTION, updateCollection),
  takeLatest(DELETE_COLLECTION, deleteCollection)
];
