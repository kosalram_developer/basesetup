import { put, call, takeLatest } from "redux-saga/effects";
import { LOGIN_REQUEST } from "../actions/actionConstants";
import Api from "../api";
import { loginSuccess, loginFail } from "../actions/login";

export function* login(action) {
  try {
    const { data } = action;
    const response = yield call(Api.login, data);
    yield put(loginSuccess(response.data));
  } catch (error) {
    console.log(error);
    yield put(loginFail(error));
  }
}

export const loginSagas = [takeLatest(LOGIN_REQUEST, login)];
