import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/assignCRUD";
import {
  fetchAssignSuccess,
  deleteAssignSuccess,
  updateAssignSuccess,
  createAssignSuccess
} from "../actions/admin";
import {
  ADD_NEW_ASSIGN,
  FETCH_ASSIGN,
  UPDATE_ASSIGN,
  DELETE_ASSIGN
} from "../actions/actionConstants";

// Assign
export function* createAssign(value) {
  try {
    console.log("Create Assign Saga Called");
    const response = yield call(Api.createassign, value);
    yield put(createAssignSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateAssign(value) {
  try {
    console.log("update Assign Saga Called");
    const response = yield call(Api.updateassign, value);
    yield put(updateAssignSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteAssign(value) {
  try {
    console.log("delete Assign Saga Called");
    yield call(Api.deleteassign, value);
    yield put(deleteAssignSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchAssign() {
  try {
    console.log("fetch Assign saga called");
    const response = yield call(Api.fetchassign);
    yield put(fetchAssignSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const assignCRUD = [
  takeLatest(ADD_NEW_ASSIGN, createAssign),
  takeLatest(FETCH_ASSIGN, fetchAssign),
  takeLatest(UPDATE_ASSIGN, updateAssign),
  takeLatest(DELETE_ASSIGN, deleteAssign)
];
