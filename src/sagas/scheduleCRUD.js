import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/scheduleCRUD";

import {
  fetchScheduleSuccess,
  deleteScheduleSuccess,
  updateScheduleSuccess,
  createScheduleSuccess
} from "../actions/admin";
import {
  ADD_NEW_SCHEDULE,
  FETCH_SCHEDULE,
  UPDATE_SCHEDULE,
  DELETE_SCHEDULE
} from "../actions/actionConstants";

// Schedule
export function* createSchedule(value) {
  try {
    console.log("Create schedule Saga Called");
    const response = yield call(Api.createschedule, value);
    yield put(createScheduleSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateSchedule(value) {
  try {
    console.log("update schedule Saga Called");
    const response = yield call(Api.updateschedule, value);
    yield put(updateScheduleSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteSchedule(value) {
  try {
    console.log("delete schedule Saga Called");
    yield call(Api.deleteschedule, value);
    yield put(deleteScheduleSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchSchedule() {
  try {
    console.log("fetch schedule saga called");
    const response = yield call(Api.fetchschedule);
    yield put(fetchScheduleSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const scheduleCRUD = [
  takeLatest(ADD_NEW_SCHEDULE, createSchedule),
  takeLatest(FETCH_SCHEDULE, fetchSchedule),
  takeLatest(UPDATE_SCHEDULE, updateSchedule),
  takeLatest(DELETE_SCHEDULE, deleteSchedule)
];
