import { put, call, takeLatest } from "redux-saga/effects";
import {
  FETCH_COMPANY,
  ADD_NEW_COMPANY,
  UPDATE_COMPANY,
  FETCH_FORM_COMPANY,
  DELETE_COMPANY
} from "../actions/actionConstants";
import Api from "../api/companyCRUD";
import {
  fetchCompanySuccess,
  createCompanySuccess,
  updateCompanySuccess,
  fetchformcompanySuccess,
  deleteCompanySuccess
} from "../actions/admin";

// Company
export function* createCompany(value) {
  try {
    yield call(Api.createcompany, value);
    yield put(createCompanySuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* updateCompany(value) {
  try {
    console.log("update Company Saga Called");
    yield call(Api.updatecompany, value);
    yield put(updateCompanySuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteCompany(value) {
  try {
    console.log("delete Company Saga Called");
    yield call(Api.deletecompany, value);
    yield put(deleteCompanySuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchCompany() {
  try {
    console.log("fetch company saga called");
    const response = yield call(Api.fetchcompany);
    yield put(fetchCompanySuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchformcompany() {
  try {
    console.log("fetch company saga called");
    const response = yield call(Api.fetchformcompany);
    yield put(fetchformcompanySuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const companyCRUD = [
  takeLatest(FETCH_COMPANY, fetchCompany),
  takeLatest(FETCH_FORM_COMPANY, fetchformcompany),
  takeLatest(ADD_NEW_COMPANY, createCompany),
  takeLatest(UPDATE_COMPANY, updateCompany),
  takeLatest(DELETE_COMPANY, deleteCompany)
];
