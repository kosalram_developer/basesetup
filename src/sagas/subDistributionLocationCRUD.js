import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/subDistributionLocationCRUD";
import {
  fetchSubDistributionLocationSuccess,
  deleteSubDistributionLocationSuccess,
  updateSubDistributionLocationSuccess,
  createSubDistributionLocationSuccess
} from "../actions/admin";
import {
  FETCH_SUBDISTRIBUTIONLOCATION,
  ADD_NEW_SUBDISTRIBUTIONLOCATION,
  UPDATE_SUBDISTRIBUTIONLOCATION,
  DELETE_SUBDISTRIBUTIONLOCATION
} from "../actions/actionConstants";

// SubDistribution Location
export function* createSubDistributionLocation(value) {
  try {
    console.log("Create Sub Distribution Location Saga Called");
    const response = yield call(Api.createsubdistributionlocation, value);
    yield put(createSubDistributionLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateSubDistributionLocation(value) {
  try {
    console.log("update SubDistributionLocation Saga Called");
    const response = yield call(Api.updatesubdistributionlocation, value);
    yield put(updateSubDistributionLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteSubDistributionLocation(value) {
  try {
    console.log("delete SubDistributionLocation Saga Called");
    yield call(Api.deletesubdistributionlocation, value);
    yield put(deleteSubDistributionLocationSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchSubDistributionLocation() {
  try {
    console.log("Fetch Sub  Distribution Location Saga Called");
    const response = yield call(Api.fetchsubdistributionlocation);
    yield put(fetchSubDistributionLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const subDistributionLocationCRUD = [
  takeLatest(FETCH_SUBDISTRIBUTIONLOCATION, fetchSubDistributionLocation),
  takeLatest(ADD_NEW_SUBDISTRIBUTIONLOCATION, createSubDistributionLocation),
  takeLatest(UPDATE_SUBDISTRIBUTIONLOCATION, updateSubDistributionLocation),
  takeLatest(DELETE_SUBDISTRIBUTIONLOCATION, deleteSubDistributionLocation)
];
