import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";

import {
  TextField
} from '@material-ui/core';
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styles/styles";
import { Redirect } from "react-router-dom";

import { loginRequest } from '../../actions/login';

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  handleOnChange = (event, param) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      [param]: event.target.value
    });
  }

  onLoginSubmit = () => {
    const { username, password } = this.state;
    this.props.login({
      username,
      password
    });
  }

  render() {
    const { classes, isLogIn } = this.props;
    const { username, password } = this.state;
    return (
      isLogIn ? (
        <Redirect to="/home" />
      ) : (
        <main className={classes.main}>
          <CssBaseline />
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Boras
            </Typography>
            <div className={classes.form}>
              <FormControl margin="normal" required fullWidth>
                <TextField
                  id="outlined-email-input"
                  label="Username"
                  className={classes.textField}
                  type="email"
                  name="username"
                  defaultValue={username}
                  onChange={(e) => this.handleOnChange(e, 'username')}
                  autoComplete="email"
                  margin="normal"
                  variant="outlined"
                  autoFocus
                />
                {/* <InputLabel>Username</InputLabel>
                <Input
                  id="username"
                  name="username"
                  autoComplete="email"
                  autoFocus
                /> */}
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <TextField
                  id="outlined-password-input"
                  label="Password"
                  className={classes.textField}
                  type="password"
                  defaultValue={password}
                  onChange={(e) => this.handleOnChange(e, 'password')}
                  autoComplete="current-password"
                  margin="normal"
                  variant="outlined"
                />
                {/* <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                  name="password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                /> */}
              </FormControl>
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.onLoginSubmit}
              >
                Sign in
              </Button>
            </div>
          </Paper>
        </main>
      )
    );
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  isLogIn: state.user.isLogIn
});
const mapDispatchToProps = dispatch => ({
  login: value => {
    dispatch(loginRequest(value));
  }
});
const LogIn = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);

export default compose(withStyles(styles))(LogIn);
