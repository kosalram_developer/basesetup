import {
  createStyles
} from '@material-ui/core/styles';

const styles = theme => createStyles({
  main: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    boxSizing: 'border-box',
    width: 500,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400
    },
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    height: '100%',
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

export default styles;