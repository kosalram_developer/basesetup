import Loadable from "react-loadable";

const loading = () => null;

export const approutes = [
  {
    path: "/home",
    component: Loadable({
      loader: () => import("../containers/Home"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/signup",
    component: Loadable({
      loader: () => import("../containers/SignUp"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/collection",
    component: Loadable({
      loader: () => import("../containers/Collection"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Company",
    component: Loadable({
      loader: () => import("../containers/Company"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Users",
    component: Loadable({
      loader: () => import("../containers/User"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Movies",
    component: Loadable({
      loader: () => import("../containers/Movies"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Assign",
    component: Loadable({
      loader: () => import("../containers/Assign"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Theatre",
    component: Loadable({
      loader: () => import("../containers/Theatre"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/DistLocation",
    component: Loadable({
      loader: () => import("../containers/DistributionLocation"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/StationLocation",
    component: Loadable({
      loader: () => import("../containers/StationLocation"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Schedule",
    component: Loadable({
      loader: () => import("../containers/Schedule"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/TheatreAgreement",
    component: Loadable({
      loader: () => import("../containers/TheatreAgreement"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/SubDistLocation",
    component: Loadable({
      loader: () => import("../containers/SubDistributionLocation"),
      loading
    }),
    isPrivate: false
  },

  {
    path: "/app/Assign",
    component: Loadable({
      loader: () => import("../containers/Assign"),
      loading
    }),
    isPrivate: false
  },
  {
    path: "/app/Report",
    component: Loadable({
      loader: () => import("../containers/Report"),
      loading
    }),
    isPrivate: false
  }
];

export const authroutes = [
  {
    path: "/",
    component: Loadable({
      loader: () => import("../pages/Login/index"),
      loading
    }),
    isPrivate: false
}];

